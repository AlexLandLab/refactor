<?php

declare(strict_types=1);

namespace App;

/**
 * [Description BazCrmConnectorFactory]
 * @package App
 */
class BazCrmConnectorFactory implements ICrmConnectorFactory
{
    
    /**
     * Set Credentials based on incoming parameters
     *
     * @param array $settings Array with Incoming settings 
     * 
     * @return CrmConnector Instance of CrmConnector
     */
    public function createConnection(array $settings): CrmConnector
    {
        $connector = new BazCrmConnector();
        $connector->setCredentials($settings);
        return $connector;
    }
}
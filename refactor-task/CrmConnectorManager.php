<?php

declare(strict_types=1);

namespace App;

use InvalidArgumentException;

/**
 * Class CrmConnectorManager
 * @package App
 */
class CrmConnectorManager
{
    /**
     * @var ICrmConnectorFactory
     */
    private ICrmConnectorFactory $factory;
    
    /**
     * @var array
     */
    private array $settings;

    /**
     * Implementing a constructor using DI ICrmConnectorFactory
     * 
     * @param array $settings Array with Incoming settings
     * 
     * @param ICrmConnectorFactory $factory ICrmConnectorFactory
     */
    public function __construct(array $settings, ICrmConnectorFactory $factory)
    {
        $this->settings = $settings;
        $this->factory = $factory;
    }

    /**
     * Sends the person to a crm
     *
     * @param array $clientEntity Client Entity
     * 
     * @return int Response code
     */
    public function sendPerson(array $clientEntity): int
    {
        $connection = $this->factory->createConnection($this->settings);
        return $connection->send($clientEntity);
    }
}

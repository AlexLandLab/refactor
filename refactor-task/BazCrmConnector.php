<?php

declare(strict_types=1);

namespace App;

/**
 * [Description BazCrmConnector]
 * @package App
 */
class BazCrmConnector extends CrmConnector
{
     /**
     * Sends data to the Baz crm
     *
     * @param array $data Array with data to send
     * 
     * @return int Server response code
     */
     public function send(array $data): int
    {
        //@todo Do not implement a logic for send specifically. Imagine that it's here.

        return 200;
    }
}

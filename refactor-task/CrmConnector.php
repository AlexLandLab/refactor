<?php

declare(strict_types=1);

namespace App;

/**
 * [Description CrmConnector]
 */
abstract class CrmConnector
{
    /**
     * Credentianals settings
     * 
     * @var array
     */
    protected array $settings = [];

    /**
     * Set Credentials based on incoming parameters
     *
     * @param array $settings Array with Incoming settings 
     * 
     * @return self Instance of current class implementation
     */
    public function setCredentials(array $settings): self
    {
        $this->settings = $settings;
        return $this;
    }

    /**
     * Sends data to the crm
     *
     * @param array $data Array with data to send
     * 
     * @return int Server response code
     */
    abstract public function send(array $data): int;
}
